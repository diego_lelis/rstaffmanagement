import { setInterval } from "timers";

const authentication = require('../auth/apiauthentication').authenticate
const authorization = require('../auth/apiauthorization')
const _ = require('lodash')
const errorTerms = require('../errorterms')

async function authHelper(req: any, resource: string, method: string) {
  try {
    // console.log('groupname', req.headers.groupname);
    let isAuth = await authentication(req)
    if (isAuth) {
      await authorization(req.headers.groupname, resource, method)
    } else {
      throw new Error(errorTerms.lackOfAuth)
    }
  } catch (e) {
    throw new Error(e.message)
  }
}

  function search(query: string) {
    return function(element: string) {
      for(let i in query) {
        if (typeof query[i] !== 'undefined' && typeof element[i] !== 'undefined') {
          if (query[i].toLowerCase() !== element[i].toLowerCase()) {
            return false
          }
        }
      }
      return true
    }
  }

  function searchStaff (users: any, query: any) {
    return users.filter(search(query))
  }
  
  function findUser(users: any, id: number): any {
    for (let user of users) {
      if (user.userId === id) {
        return user
      }
    }
    return -1
  }

  const getAutoincrement = (users: any) => users[users.length - 1].userId + 1

  function createErrorMsg(error) {
    return {message: error.message}
  }

  function createPage(users, sizePage, req) {
    //pagination
    let maxPages = Math.ceil(users.length / sizePage)
    let currentPage = 0
    if (!_.isEmpty(req.params.page)) {
      currentPage = parseInt(req.params.page) - 1
    }
    if (currentPage < 0 || currentPage > maxPages) {
      throw new Error(`${errorTerms.invalidPage}${maxPages}`)
    }
    let startIndex = currentPage * sizePage
    users = users.slice(startIndex, startIndex + sizePage)
    let nextPageNumber = currentPage + 2
    let nextPage = null
    if (nextPageNumber <= maxPages) {
      nextPage = `http://${req.headers.host}`
      if (_.isEmpty(req.query)) {
        // console.log('empty');
        nextPage = `${nextPage}${req.path.replace(req.params.page, '')}${nextPageNumber}`
      } else {
        nextPage = `${nextPage}${req.path.replace(req.params.page, '')}/${nextPageNumber}`
        let queryItems = []
        for (prop in req.query) {
          if (req.query.hasOwnProperty(prop)) {
            queryItems.push(`${prop}=${req.query[prop]}`)
          }
        }
        let stringQuery = queryItems.join('&')
        nextPage = `${nextPage}?${stringQuery}`
      }
      
    }
    return {items: users, next: nextPage}
  }

  function createPageContext(pageInfo, req) {
    let nextPage = null
    let maxPage = Math.ceil(pageInfo.countDB / pageInfo.sizePage)
    let nextPageNumber = pageInfo.currentPage + 2
    let externalPageNumber = nextPageNumber - 1
    let page = `${externalPageNumber} of ${maxPage}`
    let countNext = pageInfo.countDB - pageInfo.offset
    if (countNext > pageInfo.sizePage) {
      nextPage = `http://${req.headers.host}/${req.path}`.replace(`/${externalPageNumber}`,'/') 
      nextPage = `${nextPage}${nextPageNumber}`
      if (!_.isEmpty(req.query)) {
        let queryItems = []
        for (let prop in req.query) {
          if (req.query.hasOwnProperty(prop)) {
            queryItems.push(`${prop}=${req.query[prop]}`)
          }
        }
        let stringQuery = queryItems.join('&')
        nextPage = `${nextPage}?${stringQuery}`
      }
    }
    return {nextPage: nextPage, page: page}
  }

  module.exports = {getAutoincrement, findUser, searchStaff, authHelper, createErrorMsg, createPageContext}