const {getAutoincrement, findUser, searchStaff, authHelper, createErrorMsg, createPageContext} = require('./helpers')
const request = require('request')
// import users from '../users'
const User = require('../models').User
const _ = require('lodash')
const sizePage = 10
const errorTerms = require('../errorterms')

async function getStaffList(req: any, res: any) {
    try {
        let isAuth = await authHelper(req, 'staff', 'list')
        let currentPage = parseInt(req.params.page) - 1 || 0
        let offset = currentPage * sizePage || 0
        let queryOptions = {
           offset: offset,
           limit: sizePage
        }
        // console.log(req.query)

        let query = await User.findAndCountAll({
            where: req.query,
            offset: queryOptions.offset,
            limit: queryOptions.limit
        })
        if (offset > query.count) {
          throw new Error(errorTerms.invalidPage)
        }
        let users = query.rows
        let pageInfo = {
          currentPage: currentPage,
          sizePage: sizePage,
          countDB: query.count,
          offset: offset
        }
        let pageContext = createPageContext(pageInfo, req)
        res.send({items: users, page: pageContext.page, next: pageContext.nextPage})
      } catch (e) {
        if (e.message.startsWith(errorTerms.invalidPage)) {
          res.status(404).send(createErrorMsg(e))
        } else {
          res.status(400).send(createErrorMsg(e))
        }
      }
}

async function readStaff(req: any, res: any) {
    try {
        let isAuth = await authHelper(req, 'staff', 'get')
        // let user = findUser(users, parseInt(req.params.id))
        let user = await User.findById(req.params.id)
        if (user == null ) {
          let e = {message: errorTerms.userNotFound}
          res.status(404).send(e)
        } else {
          res.send(user)
        }
      } catch (e) {
        res.status(400).send(createErrorMsg(e))
      }
}


async function postStaff(req: any, res: any) {
    // res.json(req.body)
    try {      
      await authHelper(req, 'staff', 'post')
      const input = req.body
      let user = await User.create(input)
      res.status(201).send(user)
    } catch (e) {
      res.status(400).send(createErrorMsg(e))
    }
}

async function postStaffBulk(req: any, res: any) {
  // res.json(req.body)
  try {
    await authHelper(req, 'staff', 'bulkPost')
    const input = req.body
    let users = await User.bulkCreate(input, { validate: true })
    res.status(201).send(users)
  } catch (e) {
    res.status(400).send(createErrorMsg(e))
  }
}

async function webhookPost(req: any, res: any) {
  try{
    await request({
      url: 'http://localhost:3000/staff',
      method: 'POST',
      json: true,
      body: req.body,
      headers: {
        authentication: req.headers.authentication, 
        groupname: req.headers.groupname}
    }, function (error: any, response: any, body: any) {
        if (error) {
          res.send(error)
        } else {
          res.status(response.statusCode).send(body)
        }
      })
  } catch (e) {
    console.log(e);
    
  }
  
}


module.exports = {getStaffList, readStaff, postStaff, postStaffBulk, webhookPost}
