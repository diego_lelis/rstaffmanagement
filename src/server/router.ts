const {getStaffList, readStaff, postStaff, postStaffBulk, webhookPost } = require('./controller')
const urlList = require('./urls')


function createRoutes(app: any) {
    app.get(urlList.staff.list, getStaffList)
    app.get(urlList.staff.read, readStaff)
    app.post(urlList.staff.base, postStaff)
    app.post(urlList.staff.webhook, webhookPost)
    app.post(urlList.staff.bulkPost, postStaffBulk)
}

module.exports = createRoutes
