const chai = require('chai')
const assert = chai.assert
import users from '../users'
import { create } from 'domain';
const request = require('supertest')
const getAutoincrement = require('../server/helpers').getAutoincrement
const urlList = require('../server/urls')
const app = require('../index').app
const createErrorMsg = require('../server/helpers').createErrorMsg
const errorTerms = require('../errorterms')

describe('Test get auto incremment file', function () {
    it('should add 1 to the id to insert new user', () => {
        let result = users[users.length - 1].userId + 1
        assert.equal(result, getAutoincrement(users))
    })
})

describe ('Test lack of authentication on all endpoints', function () {
    before(
        for (let prop in urlList.staff) {
            if (urlList.staff.hasOwnProperty(prop)) {
                urlList.staff[prop] = urlList.staff[prop]
                    .replace(`:page?`, '').replace(':id', '1')
            }
        }
    )
    delete(urlList.staff.base)
    for (let prop in urlList.staff) {
        if (urlList.staff.hasOwnProperty(prop)) {
            if (prop === 'read' || prop === 'list') {
                it(`should give error lack of auth error on ${urlList.staff[prop]} endpoint`, (done: any) => {
                    request(app)
                        .get(urlList.staff[prop])
                        .expect(400)
                        .end((err, res) => {
                            if (err) throw err                
                            let e = {message: errorTerms.lackOfAuth}
                            assert.deepEqual(res.body, createErrorMsg(e))
                            done()
                        })
                })
            } else if (prop === 'post' || prop === 'webhook') {
                it(`should give error on ${urlList.staff[prop]} endpoint`, (done: any) => {
                    request(app)
                        .post(urlList.staff[prop])
                        .expect(400)
                        .end((err, res) => {
                            if (err) throw err                
                            let e = {message: errorTerms.lackOfAuth}
                            assert.deepEqual(res.body, createErrorMsg(e))
                            done()
                        })
                })
            }
        }
    }
})






