'use strict'
const groupList = require('./grouplist')
const GroupPermission = require('../models').GroupPermission

async function findGroupPermissions(groupName: string, resource: string) {
  // console.log(groupName);
  
  try {
    let group = await GroupPermission.findOne({
      where: { 
        groupname: groupName
      },
      attributes: [resource]
    })
    // console.log('group', group);
    
    if (group == null)  {
      return null
    }
    let permissions = group.dataValues[resource]

    return permissions
  } catch (e) {
    throw new Error(e.message)
  }
  
}

async function checkAuthorization(groupName: string, resource: string, action: string) {
  try {
    let permissions = await findGroupPermissions(groupName, resource)
    if (permissions == null) {
      throw new Error(`The resource ${resource}:${action} doesn't exist`)
    } else {
      let permissionsArr = permissions.split(' ')
      if (!(permissionsArr.includes(action))) {
        throw new Error(`This user don't have enough permissions to perform this action`)
      }
    }
  } catch (e) {
    throw new Error(e.message)
  }

}

module.exports = checkAuthorization
