// const apiKList = require('./apikeylist')
const _ = require('lodash')
const bcrypt = require('bcrypt-nodejs')
const errorTerms = require('../errorterms')
const ApiKey = require('../models').ApiKey
const apiKeyListHashed: Array<string> = []

function hashKey(key) {
     return bcrypt.hashSync(key, bcrypt.genSaltSync(1), null)
}

// let hash1 = hashKey('apikeytest')

async function getAPIKeyItem(group: string) {
    try {
        let item = await ApiKey.findOne(
            {
            where: { group: group }
            }
        )
        if (item == null) {
            return null
        }
        return item.dataValues
    } catch (e) {
        throw new Error(e.message)
    }
}
async function validateApiKey(key, item) {
    // console.log(key, item);
    const isAuth = await new Promise((resolve, reject) => {
        bcrypt.compare(key, item.key, function(err, res) {
          if (err) reject(err)
          resolve(res)
        });
      })
    
      return isAuth
    }
}

async function authenticate(req: any) {    
    try {
        if (!_.has(req.headers, 'authentication') || !_.has(req.headers, 'groupname')) {
            throw new Error(errorTerms.lackOfAuth)
        }
        let groupItem = await getAPIKeyItem(req.headers.groupname)
        if (groupItem == null) {
            throw new Error(errorTerms.lackOfAuth)
        }
        let isAuth = await validateApiKey(req.headers.authentication, groupItem)
        if (!isAuth) {
            throw new Error(errorTerms.invalidAPIKey)
        }
        return isAuth
    } catch (e) {
        throw new Error(e.message)
    }
    
}


module.exports = {authenticate, hashKey}
