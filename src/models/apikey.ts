'use strict';
module.exports = (sequelize, DataTypes) => {
  const ApiKey = sequelize.define('ApiKey', {
    key: {
     type: DataTypes.STRING,
     allowNull: false,
     unique: true
    },
    group: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
     }
  }, {});
  ApiKey.associate = function(models) {
    // associations can be defined here
  };
  return ApiKey;
};