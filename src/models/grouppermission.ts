'use strict';
module.exports = (sequelize, DataTypes) => {
  const GroupPermission = sequelize.define('GroupPermission', {
    groupname: {type: DataTypes.STRING, 
      unique: true,
      allowNull: {
      args: false,
      msg: "Group name can't be empty"
    }},
    staff: DataTypes.STRING
  }, {});
  GroupPermission.associate = function(models) {
    // associations can be defined here
  };
  return GroupPermission;
};