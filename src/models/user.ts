'use strict'
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    firstname: {type: DataTypes.STRING, allowNull: {
      args: false,
      msg: "first name can't be empty"
    }},
    lastname: {type: DataTypes.STRING, allowNull: {
      args: false,
      msg: "last name can't be empty"
    }},
    email: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: "email can't be empty"
      },
      unique: {
        args: true,
        msg: 'duplicated email'
      },
      validate: {isEmail: {
        args: true,
        msg: 'Invalid email'
      }}
    },
    gender: {type: DataTypes.STRING, allowNull: false, validate: 
      {isIn: 
        {args: [['Male', 'Female']],
        msg: 'Invalid sex'
      }}},
    ip: {type: DataTypes.STRING, allowNull: false}
  }, {timestamps: false})
  User.associate = function (models) {
    // associations can be defined here
  }
  return User
}
