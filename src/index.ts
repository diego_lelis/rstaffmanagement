import * as express from 'express'
import * as bodyParser from 'body-parser'
const router = require('./server/router')

const app = express()
const port = 3000

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

router(app)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))


module.exports = {app}