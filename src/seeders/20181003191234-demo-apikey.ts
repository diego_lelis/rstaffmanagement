'use strict'
import apiKeys from '../auth/apikeylist'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ApiKeys', apiKeys, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ApiKeys', null, {})
  }
}
