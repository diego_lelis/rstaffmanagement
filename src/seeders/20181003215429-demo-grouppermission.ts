'use strict';
import grouppermissions from '../auth/grouplist'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('GroupPermissions', grouppermissions, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('GroupPermissions', null, {})
  }
};
