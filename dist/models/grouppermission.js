'use strict';
module.exports = function (sequelize, DataTypes) {
    var GroupPermission = sequelize.define('GroupPermission', {
        groupname: { type: DataTypes.STRING,
            unique: true,
            allowNull: {
                args: false,
                msg: "Group name can't be empty"
            } },
        staff: DataTypes.STRING
    }, {});
    GroupPermission.associate = function (models) {
        // associations can be defined here
    };
    return GroupPermission;
};
//# sourceMappingURL=grouppermission.js.map