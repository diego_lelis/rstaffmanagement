'use strict';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('GroupPermissions', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            groupname: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            staff: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
                defaultValue: Sequelize.fn('NOW')
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
                defaultValue: Sequelize.fn('NOW')
            }
        });
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('GroupPermissions');
    }
};
//# sourceMappingURL=20181003215354-create-group-permission.js.map