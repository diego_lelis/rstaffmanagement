Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var router = require('./server/router');
var app = express();
var port = 3000;
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
router(app);
app.listen(port, function () { return console.log("Example app listening on port " + port + "!"); });
module.exports = { app: app };
//# sourceMappingURL=index.js.map