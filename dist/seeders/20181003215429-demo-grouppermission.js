'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var grouplist_1 = require("../auth/grouplist");
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.bulkInsert('GroupPermissions', grouplist_1.default, {});
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('GroupPermissions', null, {});
    }
};
//# sourceMappingURL=20181003215429-demo-grouppermission.js.map