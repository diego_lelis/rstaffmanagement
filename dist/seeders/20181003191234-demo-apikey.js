'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var apikeylist_1 = require("../auth/apikeylist");
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.bulkInsert('ApiKeys', apikeylist_1.default, {});
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('ApiKeys', null, {});
    }
};
//# sourceMappingURL=20181003191234-demo-apikey.js.map