'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var users_1 = require("../users");
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.bulkInsert('Users', users_1.default, {});
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
//# sourceMappingURL=20181003064943-demo-user.js.map