Object.defineProperty(exports, "__esModule", { value: true });
var chai = require('chai');
var assert = chai.assert;
var users_1 = require("../users");
var request = require('supertest');
var getAutoincrement = require('../server/helpers').getAutoincrement;
var urlList = require('../server/urls');
var app = require('../index').app;
var createErrorMsg = require('../server/helpers').createErrorMsg;
var errorTerms = require('../errorterms');
describe('Test get auto incremment file', function () {
    it('should add 1 to the id to insert new user', function () {
        var result = users_1.default[users_1.default.length - 1].userId + 1;
        assert.equal(result, getAutoincrement(users_1.default));
    });
});
describe('Test lack of authentication on all endpoints', function () {
    before();
    for (var prop in urlList.staff) {
        if (urlList.staff.hasOwnProperty(prop)) {
            urlList.staff[prop] = urlList.staff[prop]
                .replace(":page?", '').replace(':id', '1');
        }
    }
});
delete (urlList.staff.base);
var _loop_1 = function (prop) {
    if (urlList.staff.hasOwnProperty(prop)) {
        if (prop === 'read' || prop === 'list') {
            it("should give error lack of auth error on " + urlList.staff[prop] + " endpoint", function (done) {
                request(app)
                    .get(urlList.staff[prop])
                    .expect(400)
                    .end(function (err, res) {
                    if (err)
                        throw err;
                    var e = { message: errorTerms.lackOfAuth };
                    assert.deepEqual(res.body, createErrorMsg(e));
                    done();
                });
            });
        }
        else if (prop === 'post' || prop === 'webhook') {
            it("should give error on " + urlList.staff[prop] + " endpoint", function (done) {
                request(app)
                    .post(urlList.staff[prop])
                    .expect(400)
                    .end(function (err, res) {
                    if (err)
                        throw err;
                    var e = { message: errorTerms.lackOfAuth };
                    assert.deepEqual(res.body, createErrorMsg(e));
                    done();
                });
            });
        }
    }
};
for (var prop in urlList.staff) {
    _loop_1(prop);
}
//# sourceMappingURL=indexTest.js.map