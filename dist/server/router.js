var _a = require('./controller'), getStaffList = _a.getStaffList, readStaff = _a.readStaff, postStaff = _a.postStaff, postStaffBulk = _a.postStaffBulk, webhookPost = _a.webhookPost;
var urlList = require('./urls');
function createRoutes(app) {
    app.get(urlList.staff.list, getStaffList);
    app.get(urlList.staff.read, readStaff);
    app.post(urlList.staff.base, postStaff);
    app.post(urlList.staff.webhook, webhookPost);
    app.post(urlList.staff.bulkPost, postStaffBulk);
}
module.exports = createRoutes;
//# sourceMappingURL=router.js.map