var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var authentication = require('../auth/apiauthentication').authenticate;
var authorization = require('../auth/apiauthorization');
var _ = require('lodash');
var errorTerms = require('../errorterms');
function authHelper(req, resource, method) {
    return __awaiter(this, void 0, void 0, function () {
        var isAuth, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 5, , 6]);
                    return [4 /*yield*/, authentication(req)];
                case 1:
                    isAuth = _a.sent();
                    if (!isAuth) return [3 /*break*/, 3];
                    return [4 /*yield*/, authorization(req.headers.groupname, resource, method)];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 4];
                case 3: throw new Error(errorTerms.lackOfAuth);
                case 4: return [3 /*break*/, 6];
                case 5:
                    e_1 = _a.sent();
                    throw new Error(e_1.message);
                case 6: return [2 /*return*/];
            }
        });
    });
}
function search(query) {
    return function (element) {
        for (var i in query) {
            if (typeof query[i] !== 'undefined' && typeof element[i] !== 'undefined') {
                if (query[i].toLowerCase() !== element[i].toLowerCase()) {
                    return false;
                }
            }
        }
        return true;
    };
}
function searchStaff(users, query) {
    return users.filter(search(query));
}
function findUser(users, id) {
    for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
        var user = users_1[_i];
        if (user.userId === id) {
            return user;
        }
    }
    return -1;
}
var getAutoincrement = function (users) { return users[users.length - 1].userId + 1; };
function createErrorMsg(error) {
    return { message: error.message };
}
function createPage(users, sizePage, req) {
    //pagination
    var maxPages = Math.ceil(users.length / sizePage);
    var currentPage = 0;
    if (!_.isEmpty(req.params.page)) {
        currentPage = parseInt(req.params.page) - 1;
    }
    if (currentPage < 0 || currentPage > maxPages) {
        throw new Error("" + errorTerms.invalidPage + maxPages);
    }
    var startIndex = currentPage * sizePage;
    users = users.slice(startIndex, startIndex + sizePage);
    var nextPageNumber = currentPage + 2;
    var nextPage = null;
    if (nextPageNumber <= maxPages) {
        nextPage = "http://" + req.headers.host;
        if (_.isEmpty(req.query)) {
            // console.log('empty');
            nextPage = "" + nextPage + req.path.replace(req.params.page, '') + nextPageNumber;
        }
        else {
            nextPage = "" + nextPage + req.path.replace(req.params.page, '') + "/" + nextPageNumber;
            var queryItems = [];
            for (prop in req.query) {
                if (req.query.hasOwnProperty(prop)) {
                    queryItems.push(prop + "=" + req.query[prop]);
                }
            }
            var stringQuery = queryItems.join('&');
            nextPage = nextPage + "?" + stringQuery;
        }
    }
    return { items: users, next: nextPage };
}
function createPageContext(pageInfo, req) {
    var nextPage = null;
    var maxPage = Math.ceil(pageInfo.countDB / pageInfo.sizePage);
    var nextPageNumber = pageInfo.currentPage + 2;
    var externalPageNumber = nextPageNumber - 1;
    var page = externalPageNumber + " of " + maxPage;
    var countNext = pageInfo.countDB - pageInfo.offset;
    if (countNext > pageInfo.sizePage) {
        nextPage = ("http://" + req.headers.host + "/" + req.path).replace("/" + externalPageNumber, '/');
        nextPage = "" + nextPage + nextPageNumber;
        if (!_.isEmpty(req.query)) {
            var queryItems = [];
            for (var prop in req.query) {
                if (req.query.hasOwnProperty(prop)) {
                    queryItems.push(prop + "=" + req.query[prop]);
                }
            }
            var stringQuery = queryItems.join('&');
            nextPage = nextPage + "?" + stringQuery;
        }
    }
    return { nextPage: nextPage, page: page };
}
module.exports = { getAutoincrement: getAutoincrement, findUser: findUser, searchStaff: searchStaff, authHelper: authHelper, createErrorMsg: createErrorMsg, createPageContext: createPageContext };
//# sourceMappingURL=helpers.js.map