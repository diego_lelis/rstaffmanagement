var staffBase = '/staff';
module.exports = {
    staff: {
        base: staffBase,
        list: staffBase + "/list/:page?",
        read: staffBase + "/:id",
        post: staffBase,
        webhook: "/webhook" + staffBase,
        bulkPost: staffBase + "/bulk"
    }
};
//# sourceMappingURL=urls.js.map