module.exports = {
    lackOfAuth: 'Authentication failed',
    invalidAPIKey: 'Authentication failed',
    userNotFound: 'user not found',
    invalidPage: 'Page number invalid',
    invalidPageDB: 'Page number invalid'
};
//# sourceMappingURL=errorterms.js.map