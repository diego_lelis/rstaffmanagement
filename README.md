# Node.js Staff management app

Staff management app using Node.js, express, typescript, with authentication and authorization, and Sequelize to communicate with MySQL database.
Automated tests with Mocha and Chai
To config MySQL database go to dist/config/config.json, the default environment is "development".

See package.json to see how to do migrations and seedings

IMPORTANT: header needs two custom parameters: {authentication: 'apikeytest', groupname: 'test'}

